const incluiImagem = (entrada) => {
    return entrada.map((elemento) => {
        return elemento;
    })
}

const detalhes = (index) => {

    document.cookie = "jogador=" + index;
   
    window.location.href = "./detalhes/detalhes.html";
}


const cria_card = () => {
    const lista = incluiImagem(jogadores);
    const elemento = lista[0];
    const minha_div = document.querySelector("#div");

    minha_div.innerHTML += `<h3>${elemento.nome}</h3>`;
    minha_div.innerHTML += `<p>${elemento.posicao}</p>`;
    minha_div.innerHTML += `<img src= "${elemento.imagem}">`;
}

const cria_card_criando_elementos = (elemento) => {
    const minha_div = document.querySelector("#div");

    const div = document.createElement("button");

    minha_div.appendChild(div);
    div.addEventListener("click", () => detalhes(elemento.ID));
    div.setAttribute("id", jogadores.indexOf(elemento));
    div.setAttribute("class", "jogador");


    const h3 = document.createElement("h3");
    h3.innerHTML = elemento.nome;
    div.appendChild(h3);

    const valor = document.createElement("p");
    valor.innerHTML = elemento.posicao;
    div.appendChild(valor);

    const img = document.createElement('img');
    img.src = `${elemento.imagem}`;
    div.appendChild(img);
    
}

const busca = (jogadores) => {
    const pesquisa = document.getElementById("buscar").value
    let nome_limpo;
    let deve_entrar;

    const nova_lista =  jogadores.filter( (elemento) => {
        nome_limpo = elemento.nome.toLowerCase();
        deve_entrar = nome_limpo.includes(pesquisa.toLowerCase());
        return deve_entrar;
    });

    return nova_lista;
}



window.onload = () =>{
    const lista = incluiImagem(jogadores)
    lista.forEach((e) => {
        cria_card_criando_elementos(e);
    })
    
}

