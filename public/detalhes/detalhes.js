const card_jogador = () => {
    const detalhes = document.querySelector("#detalhes")
    detalhes.innerHTML += `
    <div id="jogador">
        <img src="${jogadores[document.cookie.split(";").find((e) => e.includes("jogador")).split("=")[1]].imagem}">
        <h2 id="nome_completo">${jogadores[document.cookie.split(";").find((e) => e.includes("jogador")).split("=")[1]].nome_completo}</h2>
        <p id="posicao">${jogadores[document.cookie.split(";").find((e) => e.includes("jogador")).split("=")[1]].posicao}</p>
        <p id="descricao">${jogadores[document.cookie.split(";").find((e) => e.includes("jogador")).split("=")[1]].descricao}</p>
        <p id="nascimento">${jogadores[document.cookie.split(";").find((e) => e.includes("jogador")).split("=")[1]].nascimento}</p>
        <p id="altura_peso">${jogadores[document.cookie.split(";").find((e) => e.includes("jogador")).split("=")[1]].altura_peso}</p>
    </div>
    `;
}

window.onload = () => {
    card_jogador()
}